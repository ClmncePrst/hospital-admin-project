/**
 * Hospital
 */
public class Hospital {
    public static void main(String[] args) {

        // Création de médicaments
        Medication med1 = new Medication("Paracétamol", "500mg");
        Medication med2 = new Medication("CureAll", "250mg");

        // Creation de maladies
        Illness illness1 = new Illness("Grippe");
        illness1.addMedication(med1);
        Illness illness2 = new Illness("Angine");
        illness2.addMedication(med2);

        // Création de patients
        Patient patient1 = new Patient("Roger Dupont", 70, "154043113125431", "R154N01");
        patient1.addIllness(illness1);
        Patient patient2 = new Patient("Sophie Lefebvre", 30, "294104526461155", "S292N02");
        patient2.addIllness(illness2);

        // Création de personnel médical
        Doctor doctor = new Doctor("Dr.Carter", 45, "145120132715509", "D007", "Urgentiste");
        Nurse nurse = new Nurse("Carla Espinosa", 27, "296089730637709234", "N001");

        //Affichage des informations des patients
        System.out.println("Patient 1 Info:");
        System.out.println(patient1.getInfo());
        System.out.println("\nPatient 2 Info:");
        System.out.println(patient2.getInfo());

        // Affichage des soins prodigués aux patients par le personnel médiacal
        doctor.careForPatient(patient1);
        nurse.careForPatient(patient2);

        // Notes dans le dossier médical des patients
        doctor.recordPatientVisit("Grippe sévère chez patient âgé(" + patient1.getAge() + "). Doubler dose de médicament (" + med1.getInfo()+")");
        nurse.recordPatientVisit("Angine virale(" + illness2.getInfo() +"). La patiente peut rentrer chez elle. ");
    }

}