public class Nurse extends MedicalStaff {
    public Nurse(String name, int age, String socialSecurityNumber, String employeeID) {
        super(name, age, socialSecurityNumber, employeeID);
    }

    @Override
    public String getRole() {
        return "Nurse";
    }

    @Override
    public void careForPatient(Patient patient) {
        System.out.println(getRole() + " " + getName() + " cares for " + patient.getName());
    }
}
