public class Doctor extends MedicalStaff {
    public String specialty;

    public Doctor(String name, int age, String socialSecurityNumber, String employeeID, String specialty) {
        super(name, age, socialSecurityNumber, employeeID);
        this.specialty = specialty;
    }

    @Override
    public String getRole() {
        return "Doctor";
    }

    @Override
    public void careForPatient(Patient patient) {
        System.out.println(getRole() + " " + getName() + " cares for " + patient.getName());
    }
}
