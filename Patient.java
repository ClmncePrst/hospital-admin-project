import java.util.ArrayList;

public class Patient extends Person {
    private String patientID;
    private ArrayList<Illness> illnessList;

    public Patient(String name, int age, String socialSecurityNumber, String patientID) {
        super(name, age, socialSecurityNumber);
        this.patientID = patientID;
        this.illnessList = new ArrayList<>();
    }

    public void addIllness(Illness illness) {
        illnessList.add(illness);
    }

    public String getInfo() {
        String info = "Patient ID: " + patientID + "\n";
        info += "Name: " + getName() + "\n";
        info += "Age: " + getAge() + "\n";
        info += "Social Security Number: " + getSocialSecurityNumber() + "\n";
        info += "Illnesses:\n";
        for (Illness illness : illnessList) {
            info += illness.getInfo() + "\n";
        }
        return info;
    }
}
