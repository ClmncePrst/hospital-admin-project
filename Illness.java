import java.util.ArrayList;

public class Illness {
    private String name;
    private ArrayList<Medication> medicationList = new ArrayList<>();

    public Illness(String name) {
        this.name = name;
    }
    public Illness(String name, ArrayList<Medication> medicationList) {
        this.name = name;
        this.medicationList= medicationList;
    }

    public void addMedication(Medication medication) {
        medicationList.add(medication);
    }


    public String getInfo() {
        String info = "Illness: " + name + "\n";
        info += "Medication: \n";
        for (Medication medication : medicationList) {
            info += medication.getInfo();
        }
        return info;
    }
}
