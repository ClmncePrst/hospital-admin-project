public abstract class MedicalStaff extends Person implements Care {
    public String employeeID;

    public MedicalStaff(String name, int age, String socialSecurityNumber, String employeeID) {
        super(name, age, socialSecurityNumber);
        this.employeeID = employeeID;
    }

    public abstract String getRole();
}